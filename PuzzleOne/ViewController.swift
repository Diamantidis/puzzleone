//
//  ViewController.swift
//  PuzzleOne
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import UIKit
import Foundation


enum ErrorCode: Int {
    
    
    case lowTemp = 2
    case highTemp = 4
    case lowBattery = 8
    case generalError = 16
    
    
    func getDescription() -> String {
        switch self.rawValue {
        case 2:
            return "- 2 Low temp"
        case 4:
            return "- 4 High temp"
        case 8:
            return "- 8 Low Battery"
        case 16:
            return "- 16 General error"
        default:
            return "uknown error"
        }
    }
}


class ViewController: UIViewController {
    
    
    @IBOutlet weak var numberTextField: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapTestNumber(_ sender: Any) {
        guard let numberTextFieldText = self.numberTextField.text, let number = Int.init(numberTextFieldText), number > 0 else {
            self.showError(title: "Error", message: "Invalid coming data")
            return
        }
        let errorsFount:(Int,[Int]) = findErrors(numberToCheck: number, posibleErrors: [2,4,8,16])
        var errorsShowable:String = errorsFount.1.compactMap { (number) -> String? in
            return ErrorCode(rawValue: number)?.getDescription()
        }.joined(separator: "\u{0085}")
        if errorsFount.0 > 0 {
            errorsShowable.append(contentsOf: "\u{0085} Value of \(errorsFount.0) unidentified")
        }
        self.showError(title: "iOT errors found", message: errorsShowable)
    }
    
    private func showError(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Close", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func findErrors(numberToCheck: Int, posibleErrors:[Int], errorsFound:[Int] = []) -> (Int,[Int]) {
        var possibleErrorsComputable = posibleErrors.sorted()
        if let biggest = possibleErrorsComputable.popLast() {
            if biggest <= numberToCheck {
                var errorsFoundMuttable = errorsFound
                errorsFoundMuttable.append(biggest)
                return findErrors(numberToCheck: numberToCheck - biggest, posibleErrors: possibleErrorsComputable, errorsFound: errorsFoundMuttable)
            } else if possibleErrorsComputable.count > 0 {
                return findErrors(numberToCheck: numberToCheck, posibleErrors: possibleErrorsComputable, errorsFound: errorsFound)
            }
        }
        return (numberToCheck, errorsFound)
    }
    
}

