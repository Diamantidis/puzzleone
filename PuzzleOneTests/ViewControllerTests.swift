//
//  ViewControllerTests.swift
//  PuzzleOneTests
//
//  Created by Vasileios Diamantidis on 13/10/2019.
//  Copyright © 2019 Vasileios Diamantidis. All rights reserved.
//

import Foundation
import XCTest
@testable import PuzzleOne

class ViewControllerTests: XCTestCase {

    var vcToTes: ViewController?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        vcToTes = ViewController()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        vcToTes = nil
    }
    
    func testfindErrorsSix() {
        let responseSix = (vcToTes?.findErrors(numberToCheck: 6, posibleErrors: [2,4,8,16]))!
        XCTAssert(responseSix == (0,[4,2]))
    }
    
    func testFindErrorsForteen() {
        let responseForteen = (vcToTes?.findErrors(numberToCheck: 14, posibleErrors: [2,4,8,16]))!
        XCTAssert(responseForteen == (0,[8,4,2]))
    }
    
    func testFindErrorsThirteen() {
        let responseThirteen = (vcToTes?.findErrors(numberToCheck: 13, posibleErrors: [2,4,8,16]))!
        XCTAssert(responseThirteen == (1,[8,4]))
    }
    
    func testFindErrorsFourThousent() {
        let responseFourThousent = (vcToTes?.findErrors(numberToCheck: 4000, posibleErrors: [2,4,8,16]))!
        XCTAssert(responseFourThousent == (3970,[16,8,4,2]))
    }
    
    
    
    
}
